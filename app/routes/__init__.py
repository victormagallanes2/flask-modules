from flask import Flask, Blueprint
from flask_restful import Api
from ..module2.views import module2
from ..user.views import User


def routes(app):
    api_bp = Blueprint('api', __name__)
    api = Api(api_bp)
    api.add_resource(User, '/user')
    app.register_blueprint(api_bp)
    app.register_blueprint(module2)