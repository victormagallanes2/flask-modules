from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app import routes


def create_app():
    app = Flask(__name__)
    app.config.from_object("config.DevelopmentConfig")
    routes.routes(app)
    return app

app = create_app()
db = SQLAlchemy(app)

from app.user.models import User

