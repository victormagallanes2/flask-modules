from flask import Blueprint 
from flask import render_template


module2 = Blueprint('mod', __name__, template_folder='templates')

@module2.route('/')
def index():
	return render_template('module2/index.html')

@module2.route('/hola')
def hola():
	return '{"result" : "hola mundo!!!"}'